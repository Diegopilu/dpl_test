/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 2106.0, "minX": 0.0, "maxY": 3123.0, "series": [{"data": [[0.0, 2106.0], [0.1, 2106.0], [0.2, 2106.0], [0.3, 2106.0], [0.4, 2106.0], [0.5, 2106.0], [0.6, 2106.0], [0.7, 2106.0], [0.8, 2106.0], [0.9, 2106.0], [1.0, 2108.0], [1.1, 2108.0], [1.2, 2108.0], [1.3, 2108.0], [1.4, 2108.0], [1.5, 2108.0], [1.6, 2108.0], [1.7, 2108.0], [1.8, 2108.0], [1.9, 2108.0], [2.0, 2113.0], [2.1, 2113.0], [2.2, 2113.0], [2.3, 2113.0], [2.4, 2113.0], [2.5, 2113.0], [2.6, 2113.0], [2.7, 2113.0], [2.8, 2113.0], [2.9, 2113.0], [3.0, 2171.0], [3.1, 2171.0], [3.2, 2171.0], [3.3, 2171.0], [3.4, 2171.0], [3.5, 2171.0], [3.6, 2171.0], [3.7, 2171.0], [3.8, 2171.0], [3.9, 2171.0], [4.0, 2184.0], [4.1, 2184.0], [4.2, 2184.0], [4.3, 2184.0], [4.4, 2184.0], [4.5, 2184.0], [4.6, 2184.0], [4.7, 2184.0], [4.8, 2184.0], [4.9, 2184.0], [5.0, 2187.0], [5.1, 2187.0], [5.2, 2187.0], [5.3, 2187.0], [5.4, 2187.0], [5.5, 2187.0], [5.6, 2187.0], [5.7, 2187.0], [5.8, 2187.0], [5.9, 2187.0], [6.0, 2203.0], [6.1, 2203.0], [6.2, 2203.0], [6.3, 2203.0], [6.4, 2203.0], [6.5, 2203.0], [6.6, 2203.0], [6.7, 2203.0], [6.8, 2203.0], [6.9, 2203.0], [7.0, 2207.0], [7.1, 2207.0], [7.2, 2207.0], [7.3, 2207.0], [7.4, 2207.0], [7.5, 2207.0], [7.6, 2207.0], [7.7, 2207.0], [7.8, 2207.0], [7.9, 2207.0], [8.0, 2210.0], [8.1, 2210.0], [8.2, 2210.0], [8.3, 2210.0], [8.4, 2210.0], [8.5, 2210.0], [8.6, 2210.0], [8.7, 2210.0], [8.8, 2210.0], [8.9, 2210.0], [9.0, 2224.0], [9.1, 2224.0], [9.2, 2224.0], [9.3, 2224.0], [9.4, 2224.0], [9.5, 2224.0], [9.6, 2224.0], [9.7, 2224.0], [9.8, 2224.0], [9.9, 2224.0], [10.0, 2243.0], [10.1, 2243.0], [10.2, 2243.0], [10.3, 2243.0], [10.4, 2243.0], [10.5, 2243.0], [10.6, 2243.0], [10.7, 2243.0], [10.8, 2243.0], [10.9, 2243.0], [11.0, 2244.0], [11.1, 2244.0], [11.2, 2244.0], [11.3, 2244.0], [11.4, 2244.0], [11.5, 2244.0], [11.6, 2244.0], [11.7, 2244.0], [11.8, 2244.0], [11.9, 2244.0], [12.0, 2259.0], [12.1, 2259.0], [12.2, 2259.0], [12.3, 2259.0], [12.4, 2259.0], [12.5, 2259.0], [12.6, 2259.0], [12.7, 2259.0], [12.8, 2259.0], [12.9, 2259.0], [13.0, 2277.0], [13.1, 2277.0], [13.2, 2277.0], [13.3, 2277.0], [13.4, 2277.0], [13.5, 2277.0], [13.6, 2277.0], [13.7, 2277.0], [13.8, 2277.0], [13.9, 2277.0], [14.0, 2277.0], [14.1, 2277.0], [14.2, 2277.0], [14.3, 2277.0], [14.4, 2277.0], [14.5, 2277.0], [14.6, 2277.0], [14.7, 2277.0], [14.8, 2277.0], [14.9, 2277.0], [15.0, 2279.0], [15.1, 2279.0], [15.2, 2279.0], [15.3, 2279.0], [15.4, 2279.0], [15.5, 2279.0], [15.6, 2279.0], [15.7, 2279.0], [15.8, 2279.0], [15.9, 2279.0], [16.0, 2285.0], [16.1, 2285.0], [16.2, 2285.0], [16.3, 2285.0], [16.4, 2285.0], [16.5, 2285.0], [16.6, 2285.0], [16.7, 2285.0], [16.8, 2285.0], [16.9, 2285.0], [17.0, 2297.0], [17.1, 2297.0], [17.2, 2297.0], [17.3, 2297.0], [17.4, 2297.0], [17.5, 2297.0], [17.6, 2297.0], [17.7, 2297.0], [17.8, 2297.0], [17.9, 2297.0], [18.0, 2311.0], [18.1, 2311.0], [18.2, 2311.0], [18.3, 2311.0], [18.4, 2311.0], [18.5, 2311.0], [18.6, 2311.0], [18.7, 2311.0], [18.8, 2311.0], [18.9, 2311.0], [19.0, 2318.0], [19.1, 2318.0], [19.2, 2318.0], [19.3, 2318.0], [19.4, 2318.0], [19.5, 2318.0], [19.6, 2318.0], [19.7, 2318.0], [19.8, 2318.0], [19.9, 2318.0], [20.0, 2336.0], [20.1, 2336.0], [20.2, 2336.0], [20.3, 2336.0], [20.4, 2336.0], [20.5, 2336.0], [20.6, 2336.0], [20.7, 2336.0], [20.8, 2336.0], [20.9, 2336.0], [21.0, 2367.0], [21.1, 2367.0], [21.2, 2367.0], [21.3, 2367.0], [21.4, 2367.0], [21.5, 2367.0], [21.6, 2367.0], [21.7, 2367.0], [21.8, 2367.0], [21.9, 2367.0], [22.0, 2388.0], [22.1, 2388.0], [22.2, 2388.0], [22.3, 2388.0], [22.4, 2388.0], [22.5, 2388.0], [22.6, 2388.0], [22.7, 2388.0], [22.8, 2388.0], [22.9, 2388.0], [23.0, 2389.0], [23.1, 2389.0], [23.2, 2389.0], [23.3, 2389.0], [23.4, 2389.0], [23.5, 2389.0], [23.6, 2389.0], [23.7, 2389.0], [23.8, 2389.0], [23.9, 2389.0], [24.0, 2414.0], [24.1, 2414.0], [24.2, 2414.0], [24.3, 2414.0], [24.4, 2414.0], [24.5, 2414.0], [24.6, 2414.0], [24.7, 2414.0], [24.8, 2414.0], [24.9, 2414.0], [25.0, 2420.0], [25.1, 2420.0], [25.2, 2420.0], [25.3, 2420.0], [25.4, 2420.0], [25.5, 2420.0], [25.6, 2420.0], [25.7, 2420.0], [25.8, 2420.0], [25.9, 2420.0], [26.0, 2425.0], [26.1, 2425.0], [26.2, 2425.0], [26.3, 2425.0], [26.4, 2425.0], [26.5, 2425.0], [26.6, 2425.0], [26.7, 2425.0], [26.8, 2425.0], [26.9, 2425.0], [27.0, 2439.0], [27.1, 2439.0], [27.2, 2439.0], [27.3, 2439.0], [27.4, 2439.0], [27.5, 2439.0], [27.6, 2439.0], [27.7, 2439.0], [27.8, 2439.0], [27.9, 2439.0], [28.0, 2440.0], [28.1, 2440.0], [28.2, 2440.0], [28.3, 2440.0], [28.4, 2440.0], [28.5, 2440.0], [28.6, 2440.0], [28.7, 2440.0], [28.8, 2440.0], [28.9, 2440.0], [29.0, 2478.0], [29.1, 2478.0], [29.2, 2478.0], [29.3, 2478.0], [29.4, 2478.0], [29.5, 2478.0], [29.6, 2478.0], [29.7, 2478.0], [29.8, 2478.0], [29.9, 2478.0], [30.0, 2485.0], [30.1, 2485.0], [30.2, 2485.0], [30.3, 2485.0], [30.4, 2485.0], [30.5, 2485.0], [30.6, 2485.0], [30.7, 2485.0], [30.8, 2485.0], [30.9, 2485.0], [31.0, 2502.0], [31.1, 2502.0], [31.2, 2502.0], [31.3, 2502.0], [31.4, 2502.0], [31.5, 2502.0], [31.6, 2502.0], [31.7, 2502.0], [31.8, 2502.0], [31.9, 2502.0], [32.0, 2506.0], [32.1, 2506.0], [32.2, 2506.0], [32.3, 2506.0], [32.4, 2506.0], [32.5, 2506.0], [32.6, 2506.0], [32.7, 2506.0], [32.8, 2506.0], [32.9, 2506.0], [33.0, 2509.0], [33.1, 2509.0], [33.2, 2509.0], [33.3, 2509.0], [33.4, 2509.0], [33.5, 2509.0], [33.6, 2509.0], [33.7, 2509.0], [33.8, 2509.0], [33.9, 2509.0], [34.0, 2510.0], [34.1, 2510.0], [34.2, 2510.0], [34.3, 2510.0], [34.4, 2510.0], [34.5, 2510.0], [34.6, 2510.0], [34.7, 2510.0], [34.8, 2510.0], [34.9, 2510.0], [35.0, 2517.0], [35.1, 2517.0], [35.2, 2517.0], [35.3, 2517.0], [35.4, 2517.0], [35.5, 2517.0], [35.6, 2517.0], [35.7, 2517.0], [35.8, 2517.0], [35.9, 2517.0], [36.0, 2517.0], [36.1, 2517.0], [36.2, 2517.0], [36.3, 2517.0], [36.4, 2517.0], [36.5, 2517.0], [36.6, 2517.0], [36.7, 2517.0], [36.8, 2517.0], [36.9, 2517.0], [37.0, 2521.0], [37.1, 2521.0], [37.2, 2521.0], [37.3, 2521.0], [37.4, 2521.0], [37.5, 2521.0], [37.6, 2521.0], [37.7, 2521.0], [37.8, 2521.0], [37.9, 2521.0], [38.0, 2522.0], [38.1, 2522.0], [38.2, 2522.0], [38.3, 2522.0], [38.4, 2522.0], [38.5, 2522.0], [38.6, 2522.0], [38.7, 2522.0], [38.8, 2522.0], [38.9, 2522.0], [39.0, 2537.0], [39.1, 2537.0], [39.2, 2537.0], [39.3, 2537.0], [39.4, 2537.0], [39.5, 2537.0], [39.6, 2537.0], [39.7, 2537.0], [39.8, 2537.0], [39.9, 2537.0], [40.0, 2538.0], [40.1, 2538.0], [40.2, 2538.0], [40.3, 2538.0], [40.4, 2538.0], [40.5, 2538.0], [40.6, 2538.0], [40.7, 2538.0], [40.8, 2538.0], [40.9, 2538.0], [41.0, 2542.0], [41.1, 2542.0], [41.2, 2542.0], [41.3, 2542.0], [41.4, 2542.0], [41.5, 2542.0], [41.6, 2542.0], [41.7, 2542.0], [41.8, 2542.0], [41.9, 2542.0], [42.0, 2547.0], [42.1, 2547.0], [42.2, 2547.0], [42.3, 2547.0], [42.4, 2547.0], [42.5, 2547.0], [42.6, 2547.0], [42.7, 2547.0], [42.8, 2547.0], [42.9, 2547.0], [43.0, 2552.0], [43.1, 2552.0], [43.2, 2552.0], [43.3, 2552.0], [43.4, 2552.0], [43.5, 2552.0], [43.6, 2552.0], [43.7, 2552.0], [43.8, 2552.0], [43.9, 2552.0], [44.0, 2560.0], [44.1, 2560.0], [44.2, 2560.0], [44.3, 2560.0], [44.4, 2560.0], [44.5, 2560.0], [44.6, 2560.0], [44.7, 2560.0], [44.8, 2560.0], [44.9, 2560.0], [45.0, 2560.0], [45.1, 2560.0], [45.2, 2560.0], [45.3, 2560.0], [45.4, 2560.0], [45.5, 2560.0], [45.6, 2560.0], [45.7, 2560.0], [45.8, 2560.0], [45.9, 2560.0], [46.0, 2564.0], [46.1, 2564.0], [46.2, 2564.0], [46.3, 2564.0], [46.4, 2564.0], [46.5, 2564.0], [46.6, 2564.0], [46.7, 2564.0], [46.8, 2564.0], [46.9, 2564.0], [47.0, 2641.0], [47.1, 2641.0], [47.2, 2641.0], [47.3, 2641.0], [47.4, 2641.0], [47.5, 2641.0], [47.6, 2641.0], [47.7, 2641.0], [47.8, 2641.0], [47.9, 2641.0], [48.0, 2646.0], [48.1, 2646.0], [48.2, 2646.0], [48.3, 2646.0], [48.4, 2646.0], [48.5, 2646.0], [48.6, 2646.0], [48.7, 2646.0], [48.8, 2646.0], [48.9, 2646.0], [49.0, 2648.0], [49.1, 2648.0], [49.2, 2648.0], [49.3, 2648.0], [49.4, 2648.0], [49.5, 2648.0], [49.6, 2648.0], [49.7, 2648.0], [49.8, 2648.0], [49.9, 2648.0], [50.0, 2649.0], [50.1, 2649.0], [50.2, 2649.0], [50.3, 2649.0], [50.4, 2649.0], [50.5, 2649.0], [50.6, 2649.0], [50.7, 2649.0], [50.8, 2649.0], [50.9, 2649.0], [51.0, 2656.0], [51.1, 2656.0], [51.2, 2656.0], [51.3, 2656.0], [51.4, 2656.0], [51.5, 2656.0], [51.6, 2656.0], [51.7, 2656.0], [51.8, 2656.0], [51.9, 2656.0], [52.0, 2674.0], [52.1, 2674.0], [52.2, 2674.0], [52.3, 2674.0], [52.4, 2674.0], [52.5, 2674.0], [52.6, 2674.0], [52.7, 2674.0], [52.8, 2674.0], [52.9, 2674.0], [53.0, 2732.0], [53.1, 2732.0], [53.2, 2732.0], [53.3, 2732.0], [53.4, 2732.0], [53.5, 2732.0], [53.6, 2732.0], [53.7, 2732.0], [53.8, 2732.0], [53.9, 2732.0], [54.0, 2737.0], [54.1, 2737.0], [54.2, 2737.0], [54.3, 2737.0], [54.4, 2737.0], [54.5, 2737.0], [54.6, 2737.0], [54.7, 2737.0], [54.8, 2737.0], [54.9, 2737.0], [55.0, 2739.0], [55.1, 2739.0], [55.2, 2739.0], [55.3, 2739.0], [55.4, 2739.0], [55.5, 2739.0], [55.6, 2739.0], [55.7, 2739.0], [55.8, 2739.0], [55.9, 2739.0], [56.0, 2748.0], [56.1, 2748.0], [56.2, 2748.0], [56.3, 2748.0], [56.4, 2748.0], [56.5, 2748.0], [56.6, 2748.0], [56.7, 2748.0], [56.8, 2748.0], [56.9, 2748.0], [57.0, 2774.0], [57.1, 2774.0], [57.2, 2774.0], [57.3, 2774.0], [57.4, 2774.0], [57.5, 2774.0], [57.6, 2774.0], [57.7, 2774.0], [57.8, 2774.0], [57.9, 2774.0], [58.0, 2774.0], [58.1, 2774.0], [58.2, 2774.0], [58.3, 2774.0], [58.4, 2774.0], [58.5, 2774.0], [58.6, 2774.0], [58.7, 2774.0], [58.8, 2774.0], [58.9, 2774.0], [59.0, 2776.0], [59.1, 2776.0], [59.2, 2776.0], [59.3, 2776.0], [59.4, 2776.0], [59.5, 2776.0], [59.6, 2776.0], [59.7, 2776.0], [59.8, 2776.0], [59.9, 2776.0], [60.0, 2786.0], [60.1, 2786.0], [60.2, 2786.0], [60.3, 2786.0], [60.4, 2786.0], [60.5, 2786.0], [60.6, 2786.0], [60.7, 2786.0], [60.8, 2786.0], [60.9, 2786.0], [61.0, 2798.0], [61.1, 2798.0], [61.2, 2798.0], [61.3, 2798.0], [61.4, 2798.0], [61.5, 2798.0], [61.6, 2798.0], [61.7, 2798.0], [61.8, 2798.0], [61.9, 2798.0], [62.0, 2815.0], [62.1, 2815.0], [62.2, 2815.0], [62.3, 2815.0], [62.4, 2815.0], [62.5, 2815.0], [62.6, 2815.0], [62.7, 2815.0], [62.8, 2815.0], [62.9, 2815.0], [63.0, 2817.0], [63.1, 2817.0], [63.2, 2817.0], [63.3, 2817.0], [63.4, 2817.0], [63.5, 2817.0], [63.6, 2817.0], [63.7, 2817.0], [63.8, 2817.0], [63.9, 2817.0], [64.0, 2824.0], [64.1, 2824.0], [64.2, 2824.0], [64.3, 2824.0], [64.4, 2824.0], [64.5, 2824.0], [64.6, 2824.0], [64.7, 2824.0], [64.8, 2824.0], [64.9, 2824.0], [65.0, 2837.0], [65.1, 2837.0], [65.2, 2837.0], [65.3, 2837.0], [65.4, 2837.0], [65.5, 2837.0], [65.6, 2837.0], [65.7, 2837.0], [65.8, 2837.0], [65.9, 2837.0], [66.0, 2845.0], [66.1, 2845.0], [66.2, 2845.0], [66.3, 2845.0], [66.4, 2845.0], [66.5, 2845.0], [66.6, 2845.0], [66.7, 2845.0], [66.8, 2845.0], [66.9, 2845.0], [67.0, 2875.0], [67.1, 2875.0], [67.2, 2875.0], [67.3, 2875.0], [67.4, 2875.0], [67.5, 2875.0], [67.6, 2875.0], [67.7, 2875.0], [67.8, 2875.0], [67.9, 2875.0], [68.0, 2880.0], [68.1, 2880.0], [68.2, 2880.0], [68.3, 2880.0], [68.4, 2880.0], [68.5, 2880.0], [68.6, 2880.0], [68.7, 2880.0], [68.8, 2880.0], [68.9, 2880.0], [69.0, 2886.0], [69.1, 2886.0], [69.2, 2886.0], [69.3, 2886.0], [69.4, 2886.0], [69.5, 2886.0], [69.6, 2886.0], [69.7, 2886.0], [69.8, 2886.0], [69.9, 2886.0], [70.0, 2890.0], [70.1, 2890.0], [70.2, 2890.0], [70.3, 2890.0], [70.4, 2890.0], [70.5, 2890.0], [70.6, 2890.0], [70.7, 2890.0], [70.8, 2890.0], [70.9, 2890.0], [71.0, 2904.0], [71.1, 2904.0], [71.2, 2904.0], [71.3, 2904.0], [71.4, 2904.0], [71.5, 2904.0], [71.6, 2904.0], [71.7, 2904.0], [71.8, 2904.0], [71.9, 2904.0], [72.0, 2922.0], [72.1, 2922.0], [72.2, 2922.0], [72.3, 2922.0], [72.4, 2922.0], [72.5, 2922.0], [72.6, 2922.0], [72.7, 2922.0], [72.8, 2922.0], [72.9, 2922.0], [73.0, 2933.0], [73.1, 2933.0], [73.2, 2933.0], [73.3, 2933.0], [73.4, 2933.0], [73.5, 2933.0], [73.6, 2933.0], [73.7, 2933.0], [73.8, 2933.0], [73.9, 2933.0], [74.0, 2942.0], [74.1, 2942.0], [74.2, 2942.0], [74.3, 2942.0], [74.4, 2942.0], [74.5, 2942.0], [74.6, 2942.0], [74.7, 2942.0], [74.8, 2942.0], [74.9, 2942.0], [75.0, 2960.0], [75.1, 2960.0], [75.2, 2960.0], [75.3, 2960.0], [75.4, 2960.0], [75.5, 2960.0], [75.6, 2960.0], [75.7, 2960.0], [75.8, 2960.0], [75.9, 2960.0], [76.0, 2967.0], [76.1, 2967.0], [76.2, 2967.0], [76.3, 2967.0], [76.4, 2967.0], [76.5, 2967.0], [76.6, 2967.0], [76.7, 2967.0], [76.8, 2967.0], [76.9, 2967.0], [77.0, 2971.0], [77.1, 2971.0], [77.2, 2971.0], [77.3, 2971.0], [77.4, 2971.0], [77.5, 2971.0], [77.6, 2971.0], [77.7, 2971.0], [77.8, 2971.0], [77.9, 2971.0], [78.0, 2977.0], [78.1, 2977.0], [78.2, 2977.0], [78.3, 2977.0], [78.4, 2977.0], [78.5, 2977.0], [78.6, 2977.0], [78.7, 2977.0], [78.8, 2977.0], [78.9, 2977.0], [79.0, 2980.0], [79.1, 2980.0], [79.2, 2980.0], [79.3, 2980.0], [79.4, 2980.0], [79.5, 2980.0], [79.6, 2980.0], [79.7, 2980.0], [79.8, 2980.0], [79.9, 2980.0], [80.0, 3014.0], [80.1, 3014.0], [80.2, 3014.0], [80.3, 3014.0], [80.4, 3014.0], [80.5, 3014.0], [80.6, 3014.0], [80.7, 3014.0], [80.8, 3014.0], [80.9, 3014.0], [81.0, 3015.0], [81.1, 3015.0], [81.2, 3015.0], [81.3, 3015.0], [81.4, 3015.0], [81.5, 3015.0], [81.6, 3015.0], [81.7, 3015.0], [81.8, 3015.0], [81.9, 3015.0], [82.0, 3018.0], [82.1, 3018.0], [82.2, 3018.0], [82.3, 3018.0], [82.4, 3018.0], [82.5, 3018.0], [82.6, 3018.0], [82.7, 3018.0], [82.8, 3018.0], [82.9, 3018.0], [83.0, 3040.0], [83.1, 3040.0], [83.2, 3040.0], [83.3, 3040.0], [83.4, 3040.0], [83.5, 3040.0], [83.6, 3040.0], [83.7, 3040.0], [83.8, 3040.0], [83.9, 3040.0], [84.0, 3047.0], [84.1, 3047.0], [84.2, 3047.0], [84.3, 3047.0], [84.4, 3047.0], [84.5, 3047.0], [84.6, 3047.0], [84.7, 3047.0], [84.8, 3047.0], [84.9, 3047.0], [85.0, 3048.0], [85.1, 3048.0], [85.2, 3048.0], [85.3, 3048.0], [85.4, 3048.0], [85.5, 3048.0], [85.6, 3048.0], [85.7, 3048.0], [85.8, 3048.0], [85.9, 3048.0], [86.0, 3060.0], [86.1, 3060.0], [86.2, 3060.0], [86.3, 3060.0], [86.4, 3060.0], [86.5, 3060.0], [86.6, 3060.0], [86.7, 3060.0], [86.8, 3060.0], [86.9, 3060.0], [87.0, 3060.0], [87.1, 3060.0], [87.2, 3060.0], [87.3, 3060.0], [87.4, 3060.0], [87.5, 3060.0], [87.6, 3060.0], [87.7, 3060.0], [87.8, 3060.0], [87.9, 3060.0], [88.0, 3078.0], [88.1, 3078.0], [88.2, 3078.0], [88.3, 3078.0], [88.4, 3078.0], [88.5, 3078.0], [88.6, 3078.0], [88.7, 3078.0], [88.8, 3078.0], [88.9, 3078.0], [89.0, 3079.0], [89.1, 3079.0], [89.2, 3079.0], [89.3, 3079.0], [89.4, 3079.0], [89.5, 3079.0], [89.6, 3079.0], [89.7, 3079.0], [89.8, 3079.0], [89.9, 3079.0], [90.0, 3081.0], [90.1, 3081.0], [90.2, 3081.0], [90.3, 3081.0], [90.4, 3081.0], [90.5, 3081.0], [90.6, 3081.0], [90.7, 3081.0], [90.8, 3081.0], [90.9, 3081.0], [91.0, 3089.0], [91.1, 3089.0], [91.2, 3089.0], [91.3, 3089.0], [91.4, 3089.0], [91.5, 3089.0], [91.6, 3089.0], [91.7, 3089.0], [91.8, 3089.0], [91.9, 3089.0], [92.0, 3100.0], [92.1, 3100.0], [92.2, 3100.0], [92.3, 3100.0], [92.4, 3100.0], [92.5, 3100.0], [92.6, 3100.0], [92.7, 3100.0], [92.8, 3100.0], [92.9, 3100.0], [93.0, 3100.0], [93.1, 3100.0], [93.2, 3100.0], [93.3, 3100.0], [93.4, 3100.0], [93.5, 3100.0], [93.6, 3100.0], [93.7, 3100.0], [93.8, 3100.0], [93.9, 3100.0], [94.0, 3102.0], [94.1, 3102.0], [94.2, 3102.0], [94.3, 3102.0], [94.4, 3102.0], [94.5, 3102.0], [94.6, 3102.0], [94.7, 3102.0], [94.8, 3102.0], [94.9, 3102.0], [95.0, 3105.0], [95.1, 3105.0], [95.2, 3105.0], [95.3, 3105.0], [95.4, 3105.0], [95.5, 3105.0], [95.6, 3105.0], [95.7, 3105.0], [95.8, 3105.0], [95.9, 3105.0], [96.0, 3108.0], [96.1, 3108.0], [96.2, 3108.0], [96.3, 3108.0], [96.4, 3108.0], [96.5, 3108.0], [96.6, 3108.0], [96.7, 3108.0], [96.8, 3108.0], [96.9, 3108.0], [97.0, 3119.0], [97.1, 3119.0], [97.2, 3119.0], [97.3, 3119.0], [97.4, 3119.0], [97.5, 3119.0], [97.6, 3119.0], [97.7, 3119.0], [97.8, 3119.0], [97.9, 3119.0], [98.0, 3121.0], [98.1, 3121.0], [98.2, 3121.0], [98.3, 3121.0], [98.4, 3121.0], [98.5, 3121.0], [98.6, 3121.0], [98.7, 3121.0], [98.8, 3121.0], [98.9, 3121.0], [99.0, 3123.0], [99.1, 3123.0], [99.2, 3123.0], [99.3, 3123.0], [99.4, 3123.0], [99.5, 3123.0], [99.6, 3123.0], [99.7, 3123.0], [99.8, 3123.0], [99.9, 3123.0]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 6.0, "minX": 2100.0, "maxY": 16.0, "series": [{"data": [[2100.0, 6.0], [2200.0, 12.0], [2300.0, 6.0], [2400.0, 7.0], [2500.0, 16.0], [2600.0, 6.0], [2700.0, 9.0], [2800.0, 9.0], [2900.0, 9.0], [3000.0, 12.0], [3100.0, 8.0]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 3100.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 30.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 70.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 30.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 70.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 71.42000000000002, "minX": 1.5491526E12, "maxY": 71.42000000000002, "series": [{"data": [[1.5491526E12, 71.42000000000002]], "isOverall": false, "label": "Parametros escenario", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5491526E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 2195.5, "minX": 1.0, "maxY": 3102.0, "series": [{"data": [[32.0, 2223.5], [41.0, 2420.0], [42.0, 2633.2], [45.0, 2816.0], [55.0, 2277.0], [54.0, 2547.0], [59.0, 2814.5], [61.0, 2773.5], [60.0, 2612.2142857142853], [63.0, 2560.0], [62.0, 2765.75], [65.0, 3060.0], [64.0, 2875.5], [66.0, 2980.0], [71.0, 2649.0], [69.0, 2990.5], [75.0, 3100.0], [74.0, 3102.0], [79.0, 2512.8888888888887], [78.0, 2808.6666666666665], [82.0, 2195.5], [87.0, 2297.0], [86.0, 2517.0], [84.0, 2573.5], [89.0, 2619.3333333333335], [95.0, 2626.8], [92.0, 2706.3333333333335], [94.0, 2774.0], [99.0, 2600.5], [98.0, 2845.0], [97.0, 3060.0], [96.0, 2880.0], [100.0, 2722.857142857143], [1.0, 2414.0], [24.0, 2311.0], [26.0, 2389.0], [28.0, 2968.0], [31.0, 2590.5]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5", "isController": false}, {"data": [[71.42000000000002, 2660.9800000000014]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 690.0166666666667, "minX": 1.5491526E12, "maxY": 908.4, "series": [{"data": [[1.5491526E12, 690.0166666666667]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.5491526E12, 908.4]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5491526E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 2660.9800000000014, "minX": 1.5491526E12, "maxY": 2660.9800000000014, "series": [{"data": [[1.5491526E12, 2660.9800000000014]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5491526E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 2660.95, "minX": 1.5491526E12, "maxY": 2660.95, "series": [{"data": [[1.5491526E12, 2660.95]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5491526E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 2511.960000000001, "minX": 1.5491526E12, "maxY": 2511.960000000001, "series": [{"data": [[1.5491526E12, 2511.960000000001]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5491526E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 2108.0, "minX": 1.5491526E12, "maxY": 3121.0, "series": [{"data": [[1.5491526E12, 3121.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.5491526E12, 2108.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.5491526E12, 3058.8]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.5491526E12, 3121.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.5491526E12, 3097.9]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5491526E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 2171.0, "minX": 1000.0, "maxY": 3040.0, "series": [{"data": [[5000.0, 2307.5], [3000.0, 2713.5], [2000.0, 2911.0], [4000.0, 2511.5], [1000.0, 2336.0], [8000.0, 2171.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[5000.0, 2738.0], [3000.0, 2904.0], [4000.0, 2753.0], [2000.0, 2702.0], [1000.0, 2669.0], [8000.0, 3040.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1, "maxX": 8000.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.create();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 2171.0, "minX": 1000.0, "maxY": 3040.0, "series": [{"data": [[5000.0, 2307.5], [3000.0, 2713.5], [2000.0, 2911.0], [4000.0, 2511.5], [1000.0, 2336.0], [8000.0, 2171.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[5000.0, 2738.0], [3000.0, 2904.0], [4000.0, 2752.0], [2000.0, 2702.0], [1000.0, 2669.0], [8000.0, 3040.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1, "maxX": 8000.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.5491526E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.5491526E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5491526E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.16666666666666666, "minX": 1.5491526E12, "maxY": 1.0, "series": [{"data": [[1.5491526E12, 0.5]], "isOverall": false, "label": "201", "isController": false}, {"data": [[1.5491526E12, 0.16666666666666666]], "isOverall": false, "label": "404", "isController": false}, {"data": [[1.5491526E12, 1.0]], "isOverall": false, "label": "429", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5491526E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.5, "minX": 1.5491526E12, "maxY": 1.1666666666666667, "series": [{"data": [[1.5491526E12, 0.5]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5-success", "isController": false}, {"data": [[1.5491526E12, 1.1666666666666667]], "isOverall": false, "label": "TopRated10 /3/movie/100/rating?api_key=baabf5e3e6dd5a46dfd4b26c5849db07&session_id=00b66caed7c98aaa2f827035cee094ec22ca8ce5-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5491526E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.5, "minX": 1.5491526E12, "maxY": 1.1666666666666667, "series": [{"data": [[1.5491526E12, 0.5]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.5491526E12, 1.1666666666666667]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5491526E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "responseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    choiceContainer.find("label").each(function(){
        this.style.color = color;
    });
}

