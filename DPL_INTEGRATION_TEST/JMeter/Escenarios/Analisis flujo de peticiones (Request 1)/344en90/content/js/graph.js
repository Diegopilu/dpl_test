/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 464.0, "minX": 0.0, "maxY": 2712.0, "series": [{"data": [[0.0, 464.0], [0.1, 464.0], [0.2, 464.0], [0.3, 465.0], [0.4, 465.0], [0.5, 465.0], [0.6, 469.0], [0.7, 469.0], [0.8, 469.0], [0.9, 470.0], [1.0, 470.0], [1.1, 470.0], [1.2, 484.0], [1.3, 484.0], [1.4, 484.0], [1.5, 485.0], [1.6, 485.0], [1.7, 485.0], [1.8, 485.0], [1.9, 485.0], [2.0, 485.0], [2.1, 485.0], [2.2, 485.0], [2.3, 485.0], [2.4, 485.0], [2.5, 485.0], [2.6, 485.0], [2.7, 486.0], [2.8, 486.0], [2.9, 486.0], [3.0, 486.0], [3.1, 486.0], [3.2, 486.0], [3.3, 486.0], [3.4, 486.0], [3.5, 486.0], [3.6, 486.0], [3.7, 486.0], [3.8, 487.0], [3.9, 487.0], [4.0, 487.0], [4.1, 487.0], [4.2, 487.0], [4.3, 487.0], [4.4, 488.0], [4.5, 488.0], [4.6, 488.0], [4.7, 488.0], [4.8, 488.0], [4.9, 488.0], [5.0, 488.0], [5.1, 488.0], [5.2, 488.0], [5.3, 488.0], [5.4, 488.0], [5.5, 488.0], [5.6, 488.0], [5.7, 488.0], [5.8, 489.0], [5.9, 489.0], [6.0, 489.0], [6.1, 489.0], [6.2, 489.0], [6.3, 489.0], [6.4, 490.0], [6.5, 490.0], [6.6, 490.0], [6.7, 490.0], [6.8, 490.0], [6.9, 490.0], [7.0, 490.0], [7.1, 490.0], [7.2, 490.0], [7.3, 490.0], [7.4, 490.0], [7.5, 490.0], [7.6, 490.0], [7.7, 490.0], [7.8, 490.0], [7.9, 490.0], [8.0, 490.0], [8.1, 490.0], [8.2, 490.0], [8.3, 490.0], [8.4, 490.0], [8.5, 490.0], [8.6, 490.0], [8.7, 491.0], [8.8, 491.0], [8.9, 491.0], [9.0, 491.0], [9.1, 491.0], [9.2, 491.0], [9.3, 491.0], [9.4, 491.0], [9.5, 491.0], [9.6, 491.0], [9.7, 491.0], [9.8, 491.0], [9.9, 491.0], [10.0, 491.0], [10.1, 491.0], [10.2, 491.0], [10.3, 491.0], [10.4, 491.0], [10.5, 491.0], [10.6, 491.0], [10.7, 491.0], [10.8, 491.0], [10.9, 491.0], [11.0, 491.0], [11.1, 491.0], [11.2, 491.0], [11.3, 491.0], [11.4, 492.0], [11.5, 492.0], [11.6, 492.0], [11.7, 492.0], [11.8, 492.0], [11.9, 492.0], [12.0, 492.0], [12.1, 492.0], [12.2, 492.0], [12.3, 492.0], [12.4, 492.0], [12.5, 492.0], [12.6, 492.0], [12.7, 492.0], [12.8, 492.0], [12.9, 492.0], [13.0, 492.0], [13.1, 492.0], [13.2, 492.0], [13.3, 492.0], [13.4, 492.0], [13.5, 492.0], [13.6, 492.0], [13.7, 493.0], [13.8, 493.0], [13.9, 493.0], [14.0, 493.0], [14.1, 493.0], [14.2, 493.0], [14.3, 493.0], [14.4, 493.0], [14.5, 493.0], [14.6, 493.0], [14.7, 493.0], [14.8, 493.0], [14.9, 493.0], [15.0, 493.0], [15.1, 493.0], [15.2, 493.0], [15.3, 493.0], [15.4, 494.0], [15.5, 494.0], [15.6, 494.0], [15.7, 494.0], [15.8, 494.0], [15.9, 494.0], [16.0, 494.0], [16.1, 494.0], [16.2, 494.0], [16.3, 494.0], [16.4, 494.0], [16.5, 494.0], [16.6, 494.0], [16.7, 494.0], [16.8, 494.0], [16.9, 494.0], [17.0, 494.0], [17.1, 494.0], [17.2, 494.0], [17.3, 494.0], [17.4, 494.0], [17.5, 494.0], [17.6, 494.0], [17.7, 495.0], [17.8, 495.0], [17.9, 495.0], [18.0, 495.0], [18.1, 495.0], [18.2, 495.0], [18.3, 495.0], [18.4, 495.0], [18.5, 495.0], [18.6, 495.0], [18.7, 495.0], [18.8, 495.0], [18.9, 495.0], [19.0, 495.0], [19.1, 495.0], [19.2, 495.0], [19.3, 495.0], [19.4, 495.0], [19.5, 495.0], [19.6, 495.0], [19.7, 495.0], [19.8, 495.0], [19.9, 495.0], [20.0, 495.0], [20.1, 495.0], [20.2, 495.0], [20.3, 495.0], [20.4, 495.0], [20.5, 495.0], [20.6, 496.0], [20.7, 496.0], [20.8, 496.0], [20.9, 496.0], [21.0, 496.0], [21.1, 496.0], [21.2, 496.0], [21.3, 496.0], [21.4, 496.0], [21.5, 496.0], [21.6, 496.0], [21.7, 496.0], [21.8, 496.0], [21.9, 496.0], [22.0, 496.0], [22.1, 496.0], [22.2, 496.0], [22.3, 496.0], [22.4, 496.0], [22.5, 496.0], [22.6, 496.0], [22.7, 496.0], [22.8, 496.0], [22.9, 496.0], [23.0, 496.0], [23.1, 496.0], [23.2, 496.0], [23.3, 496.0], [23.4, 496.0], [23.5, 496.0], [23.6, 496.0], [23.7, 496.0], [23.8, 497.0], [23.9, 497.0], [24.0, 497.0], [24.1, 497.0], [24.2, 497.0], [24.3, 497.0], [24.4, 497.0], [24.5, 497.0], [24.6, 497.0], [24.7, 497.0], [24.8, 497.0], [24.9, 497.0], [25.0, 497.0], [25.1, 497.0], [25.2, 497.0], [25.3, 498.0], [25.4, 498.0], [25.5, 498.0], [25.6, 498.0], [25.7, 498.0], [25.8, 498.0], [25.9, 498.0], [26.0, 498.0], [26.1, 498.0], [26.2, 498.0], [26.3, 498.0], [26.4, 498.0], [26.5, 498.0], [26.6, 498.0], [26.7, 499.0], [26.8, 499.0], [26.9, 499.0], [27.0, 499.0], [27.1, 499.0], [27.2, 499.0], [27.3, 499.0], [27.4, 499.0], [27.5, 499.0], [27.6, 499.0], [27.7, 499.0], [27.8, 499.0], [27.9, 500.0], [28.0, 500.0], [28.1, 500.0], [28.2, 500.0], [28.3, 500.0], [28.4, 500.0], [28.5, 500.0], [28.6, 500.0], [28.7, 500.0], [28.8, 500.0], [28.9, 500.0], [29.0, 500.0], [29.1, 500.0], [29.2, 500.0], [29.3, 501.0], [29.4, 501.0], [29.5, 501.0], [29.6, 501.0], [29.7, 501.0], [29.8, 501.0], [29.9, 501.0], [30.0, 501.0], [30.1, 501.0], [30.2, 501.0], [30.3, 501.0], [30.4, 501.0], [30.5, 501.0], [30.6, 501.0], [30.7, 501.0], [30.8, 501.0], [30.9, 501.0], [31.0, 501.0], [31.1, 501.0], [31.2, 501.0], [31.3, 501.0], [31.4, 501.0], [31.5, 501.0], [31.6, 501.0], [31.7, 501.0], [31.8, 501.0], [31.9, 501.0], [32.0, 501.0], [32.1, 501.0], [32.2, 501.0], [32.3, 501.0], [32.4, 501.0], [32.5, 501.0], [32.6, 501.0], [32.7, 501.0], [32.8, 501.0], [32.9, 501.0], [33.0, 501.0], [33.1, 502.0], [33.2, 502.0], [33.3, 502.0], [33.4, 502.0], [33.5, 502.0], [33.6, 502.0], [33.7, 503.0], [33.8, 503.0], [33.9, 503.0], [34.0, 503.0], [34.1, 503.0], [34.2, 503.0], [34.3, 503.0], [34.4, 503.0], [34.5, 503.0], [34.6, 503.0], [34.7, 503.0], [34.8, 503.0], [34.9, 503.0], [35.0, 503.0], [35.1, 503.0], [35.2, 503.0], [35.3, 503.0], [35.4, 503.0], [35.5, 503.0], [35.6, 503.0], [35.7, 503.0], [35.8, 503.0], [35.9, 503.0], [36.0, 503.0], [36.1, 503.0], [36.2, 503.0], [36.3, 503.0], [36.4, 503.0], [36.5, 503.0], [36.6, 504.0], [36.7, 504.0], [36.8, 504.0], [36.9, 504.0], [37.0, 504.0], [37.1, 504.0], [37.2, 504.0], [37.3, 504.0], [37.4, 504.0], [37.5, 504.0], [37.6, 504.0], [37.7, 504.0], [37.8, 504.0], [37.9, 504.0], [38.0, 504.0], [38.1, 504.0], [38.2, 504.0], [38.3, 504.0], [38.4, 504.0], [38.5, 504.0], [38.6, 504.0], [38.7, 504.0], [38.8, 504.0], [38.9, 504.0], [39.0, 504.0], [39.1, 504.0], [39.2, 504.0], [39.3, 504.0], [39.4, 504.0], [39.5, 504.0], [39.6, 504.0], [39.7, 504.0], [39.8, 505.0], [39.9, 505.0], [40.0, 505.0], [40.1, 505.0], [40.2, 505.0], [40.3, 505.0], [40.4, 505.0], [40.5, 505.0], [40.6, 505.0], [40.7, 505.0], [40.8, 505.0], [40.9, 505.0], [41.0, 505.0], [41.1, 505.0], [41.2, 505.0], [41.3, 505.0], [41.4, 505.0], [41.5, 505.0], [41.6, 505.0], [41.7, 505.0], [41.8, 505.0], [41.9, 505.0], [42.0, 505.0], [42.1, 505.0], [42.2, 505.0], [42.3, 505.0], [42.4, 505.0], [42.5, 505.0], [42.6, 505.0], [42.7, 505.0], [42.8, 505.0], [42.9, 505.0], [43.0, 505.0], [43.1, 505.0], [43.2, 505.0], [43.3, 505.0], [43.4, 505.0], [43.5, 505.0], [43.6, 505.0], [43.7, 505.0], [43.8, 505.0], [43.9, 505.0], [44.0, 505.0], [44.1, 506.0], [44.2, 506.0], [44.3, 506.0], [44.4, 506.0], [44.5, 506.0], [44.6, 506.0], [44.7, 506.0], [44.8, 506.0], [44.9, 506.0], [45.0, 506.0], [45.1, 506.0], [45.2, 506.0], [45.3, 506.0], [45.4, 506.0], [45.5, 506.0], [45.6, 507.0], [45.7, 507.0], [45.8, 507.0], [45.9, 507.0], [46.0, 507.0], [46.1, 507.0], [46.2, 507.0], [46.3, 507.0], [46.4, 507.0], [46.5, 507.0], [46.6, 507.0], [46.7, 507.0], [46.8, 507.0], [46.9, 507.0], [47.0, 507.0], [47.1, 507.0], [47.2, 507.0], [47.3, 508.0], [47.4, 508.0], [47.5, 508.0], [47.6, 508.0], [47.7, 508.0], [47.8, 508.0], [47.9, 508.0], [48.0, 508.0], [48.1, 508.0], [48.2, 508.0], [48.3, 508.0], [48.4, 508.0], [48.5, 508.0], [48.6, 508.0], [48.7, 508.0], [48.8, 508.0], [48.9, 508.0], [49.0, 508.0], [49.1, 508.0], [49.2, 508.0], [49.3, 508.0], [49.4, 508.0], [49.5, 508.0], [49.6, 508.0], [49.7, 508.0], [49.8, 508.0], [49.9, 508.0], [50.0, 508.0], [50.1, 508.0], [50.2, 508.0], [50.3, 508.0], [50.4, 508.0], [50.5, 508.0], [50.6, 508.0], [50.7, 508.0], [50.8, 509.0], [50.9, 509.0], [51.0, 509.0], [51.1, 509.0], [51.2, 509.0], [51.3, 509.0], [51.4, 509.0], [51.5, 509.0], [51.6, 509.0], [51.7, 509.0], [51.8, 509.0], [51.9, 509.0], [52.0, 509.0], [52.1, 509.0], [52.2, 509.0], [52.3, 509.0], [52.4, 509.0], [52.5, 509.0], [52.6, 509.0], [52.7, 509.0], [52.8, 509.0], [52.9, 509.0], [53.0, 509.0], [53.1, 509.0], [53.2, 509.0], [53.3, 509.0], [53.4, 509.0], [53.5, 509.0], [53.6, 509.0], [53.7, 509.0], [53.8, 509.0], [53.9, 509.0], [54.0, 509.0], [54.1, 509.0], [54.2, 509.0], [54.3, 509.0], [54.4, 509.0], [54.5, 509.0], [54.6, 509.0], [54.7, 509.0], [54.8, 509.0], [54.9, 509.0], [55.0, 509.0], [55.1, 509.0], [55.2, 509.0], [55.3, 509.0], [55.4, 509.0], [55.5, 509.0], [55.6, 509.0], [55.7, 509.0], [55.8, 509.0], [55.9, 509.0], [56.0, 509.0], [56.1, 509.0], [56.2, 509.0], [56.3, 510.0], [56.4, 510.0], [56.5, 510.0], [56.6, 510.0], [56.7, 510.0], [56.8, 510.0], [56.9, 510.0], [57.0, 510.0], [57.1, 510.0], [57.2, 510.0], [57.3, 510.0], [57.4, 510.0], [57.5, 510.0], [57.6, 510.0], [57.7, 510.0], [57.8, 510.0], [57.9, 510.0], [58.0, 510.0], [58.1, 510.0], [58.2, 510.0], [58.3, 510.0], [58.4, 510.0], [58.5, 510.0], [58.6, 510.0], [58.7, 510.0], [58.8, 510.0], [58.9, 511.0], [59.0, 511.0], [59.1, 511.0], [59.2, 511.0], [59.3, 511.0], [59.4, 511.0], [59.5, 511.0], [59.6, 511.0], [59.7, 511.0], [59.8, 511.0], [59.9, 511.0], [60.0, 511.0], [60.1, 511.0], [60.2, 511.0], [60.3, 511.0], [60.4, 511.0], [60.5, 511.0], [60.6, 511.0], [60.7, 511.0], [60.8, 511.0], [60.9, 511.0], [61.0, 511.0], [61.1, 511.0], [61.2, 512.0], [61.3, 512.0], [61.4, 512.0], [61.5, 512.0], [61.6, 512.0], [61.7, 512.0], [61.8, 512.0], [61.9, 512.0], [62.0, 512.0], [62.1, 513.0], [62.2, 513.0], [62.3, 513.0], [62.4, 513.0], [62.5, 513.0], [62.6, 513.0], [62.7, 513.0], [62.8, 513.0], [62.9, 513.0], [63.0, 513.0], [63.1, 513.0], [63.2, 513.0], [63.3, 513.0], [63.4, 513.0], [63.5, 513.0], [63.6, 513.0], [63.7, 513.0], [63.8, 514.0], [63.9, 514.0], [64.0, 514.0], [64.1, 514.0], [64.2, 514.0], [64.3, 514.0], [64.4, 514.0], [64.5, 514.0], [64.6, 514.0], [64.7, 514.0], [64.8, 514.0], [64.9, 514.0], [65.0, 514.0], [65.1, 514.0], [65.2, 514.0], [65.3, 514.0], [65.4, 514.0], [65.5, 514.0], [65.6, 514.0], [65.7, 514.0], [65.8, 515.0], [65.9, 515.0], [66.0, 515.0], [66.1, 515.0], [66.2, 515.0], [66.3, 515.0], [66.4, 515.0], [66.5, 515.0], [66.6, 515.0], [66.7, 515.0], [66.8, 515.0], [66.9, 515.0], [67.0, 515.0], [67.1, 515.0], [67.2, 515.0], [67.3, 515.0], [67.4, 515.0], [67.5, 515.0], [67.6, 515.0], [67.7, 515.0], [67.8, 515.0], [67.9, 516.0], [68.0, 516.0], [68.1, 516.0], [68.2, 516.0], [68.3, 516.0], [68.4, 516.0], [68.5, 516.0], [68.6, 516.0], [68.7, 516.0], [68.8, 516.0], [68.9, 516.0], [69.0, 516.0], [69.1, 516.0], [69.2, 516.0], [69.3, 516.0], [69.4, 516.0], [69.5, 516.0], [69.6, 516.0], [69.7, 516.0], [69.8, 516.0], [69.9, 516.0], [70.0, 516.0], [70.1, 516.0], [70.2, 516.0], [70.3, 516.0], [70.4, 516.0], [70.5, 517.0], [70.6, 517.0], [70.7, 517.0], [70.8, 517.0], [70.9, 517.0], [71.0, 517.0], [71.1, 517.0], [71.2, 517.0], [71.3, 517.0], [71.4, 517.0], [71.5, 517.0], [71.6, 517.0], [71.7, 517.0], [71.8, 517.0], [71.9, 517.0], [72.0, 517.0], [72.1, 517.0], [72.2, 517.0], [72.3, 517.0], [72.4, 517.0], [72.5, 518.0], [72.6, 518.0], [72.7, 518.0], [72.8, 518.0], [72.9, 518.0], [73.0, 518.0], [73.1, 518.0], [73.2, 518.0], [73.3, 518.0], [73.4, 518.0], [73.5, 518.0], [73.6, 518.0], [73.7, 518.0], [73.8, 518.0], [73.9, 518.0], [74.0, 518.0], [74.1, 518.0], [74.2, 518.0], [74.3, 518.0], [74.4, 518.0], [74.5, 518.0], [74.6, 518.0], [74.7, 518.0], [74.8, 518.0], [74.9, 518.0], [75.0, 518.0], [75.1, 518.0], [75.2, 518.0], [75.3, 518.0], [75.4, 518.0], [75.5, 518.0], [75.6, 518.0], [75.7, 519.0], [75.8, 519.0], [75.9, 519.0], [76.0, 519.0], [76.1, 519.0], [76.2, 519.0], [76.3, 519.0], [76.4, 519.0], [76.5, 519.0], [76.6, 519.0], [76.7, 519.0], [76.8, 519.0], [76.9, 519.0], [77.0, 519.0], [77.1, 519.0], [77.2, 519.0], [77.3, 519.0], [77.4, 520.0], [77.5, 520.0], [77.6, 520.0], [77.7, 520.0], [77.8, 520.0], [77.9, 520.0], [78.0, 520.0], [78.1, 520.0], [78.2, 520.0], [78.3, 520.0], [78.4, 520.0], [78.5, 520.0], [78.6, 520.0], [78.7, 520.0], [78.8, 520.0], [78.9, 520.0], [79.0, 520.0], [79.1, 520.0], [79.2, 521.0], [79.3, 521.0], [79.4, 521.0], [79.5, 521.0], [79.6, 521.0], [79.7, 521.0], [79.8, 522.0], [79.9, 522.0], [80.0, 522.0], [80.1, 522.0], [80.2, 522.0], [80.3, 523.0], [80.4, 523.0], [80.5, 523.0], [80.6, 523.0], [80.7, 523.0], [80.8, 523.0], [80.9, 523.0], [81.0, 523.0], [81.1, 523.0], [81.2, 524.0], [81.3, 524.0], [81.4, 524.0], [81.5, 524.0], [81.6, 524.0], [81.7, 524.0], [81.8, 524.0], [81.9, 524.0], [82.0, 524.0], [82.1, 525.0], [82.2, 525.0], [82.3, 525.0], [82.4, 525.0], [82.5, 525.0], [82.6, 525.0], [82.7, 525.0], [82.8, 525.0], [82.9, 525.0], [83.0, 525.0], [83.1, 525.0], [83.2, 526.0], [83.3, 526.0], [83.4, 526.0], [83.5, 526.0], [83.6, 526.0], [83.7, 526.0], [83.8, 526.0], [83.9, 526.0], [84.0, 526.0], [84.1, 526.0], [84.2, 526.0], [84.3, 526.0], [84.4, 526.0], [84.5, 526.0], [84.6, 526.0], [84.7, 527.0], [84.8, 527.0], [84.9, 527.0], [85.0, 527.0], [85.1, 527.0], [85.2, 527.0], [85.3, 527.0], [85.4, 527.0], [85.5, 527.0], [85.6, 527.0], [85.7, 527.0], [85.8, 527.0], [85.9, 527.0], [86.0, 527.0], [86.1, 527.0], [86.2, 527.0], [86.3, 527.0], [86.4, 527.0], [86.5, 527.0], [86.6, 527.0], [86.7, 528.0], [86.8, 528.0], [86.9, 528.0], [87.0, 528.0], [87.1, 528.0], [87.2, 528.0], [87.3, 529.0], [87.4, 529.0], [87.5, 529.0], [87.6, 531.0], [87.7, 531.0], [87.8, 531.0], [87.9, 531.0], [88.0, 531.0], [88.1, 531.0], [88.2, 531.0], [88.3, 531.0], [88.4, 531.0], [88.5, 532.0], [88.6, 532.0], [88.7, 533.0], [88.8, 533.0], [88.9, 533.0], [89.0, 534.0], [89.1, 534.0], [89.2, 534.0], [89.3, 534.0], [89.4, 534.0], [89.5, 534.0], [89.6, 539.0], [89.7, 539.0], [89.8, 539.0], [89.9, 541.0], [90.0, 541.0], [90.1, 541.0], [90.2, 541.0], [90.3, 541.0], [90.4, 541.0], [90.5, 542.0], [90.6, 542.0], [90.7, 542.0], [90.8, 542.0], [90.9, 542.0], [91.0, 542.0], [91.1, 545.0], [91.2, 545.0], [91.3, 545.0], [91.4, 546.0], [91.5, 546.0], [91.6, 562.0], [91.7, 562.0], [91.8, 562.0], [91.9, 569.0], [92.0, 569.0], [92.1, 569.0], [92.2, 570.0], [92.3, 570.0], [92.4, 570.0], [92.5, 671.0], [92.6, 671.0], [92.7, 671.0], [92.8, 676.0], [92.9, 676.0], [93.0, 676.0], [93.1, 712.0], [93.2, 712.0], [93.3, 712.0], [93.4, 749.0], [93.5, 749.0], [93.6, 749.0], [93.7, 774.0], [93.8, 774.0], [93.9, 774.0], [94.0, 831.0], [94.1, 831.0], [94.2, 831.0], [94.3, 866.0], [94.4, 866.0], [94.5, 969.0], [94.6, 969.0], [94.7, 969.0], [94.8, 1058.0], [94.9, 1058.0], [95.0, 1058.0], [95.1, 1068.0], [95.2, 1068.0], [95.3, 1068.0], [95.4, 1093.0], [95.5, 1093.0], [95.6, 1093.0], [95.7, 1098.0], [95.8, 1098.0], [95.9, 1098.0], [96.0, 1117.0], [96.1, 1117.0], [96.2, 1117.0], [96.3, 1121.0], [96.4, 1121.0], [96.5, 1121.0], [96.6, 1131.0], [96.7, 1131.0], [96.8, 1131.0], [96.9, 1180.0], [97.0, 1180.0], [97.1, 1180.0], [97.2, 1181.0], [97.3, 1181.0], [97.4, 1240.0], [97.5, 1240.0], [97.6, 1240.0], [97.7, 1244.0], [97.8, 1244.0], [97.9, 1244.0], [98.0, 1261.0], [98.1, 1261.0], [98.2, 1261.0], [98.3, 1410.0], [98.4, 1410.0], [98.5, 1410.0], [98.6, 1682.0], [98.7, 1682.0], [98.8, 1682.0], [98.9, 1984.0], [99.0, 1984.0], [99.1, 1984.0], [99.2, 2321.0], [99.3, 2321.0], [99.4, 2321.0], [99.5, 2560.0], [99.6, 2560.0], [99.7, 2560.0], [99.8, 2712.0], [99.9, 2712.0]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 400.0, "maxY": 223.0, "series": [{"data": [[2300.0, 1.0], [600.0, 2.0], [2500.0, 1.0], [2700.0, 1.0], [700.0, 3.0], [800.0, 2.0], [900.0, 1.0], [1000.0, 4.0], [1100.0, 5.0], [1200.0, 3.0], [1400.0, 1.0], [1600.0, 1.0], [400.0, 96.0], [1900.0, 1.0], [500.0, 223.0]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 2700.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 5.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 237.0, "series": [{"data": [[0.0, 95.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 237.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 5.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 8.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 2.3, "minX": 1.54907484E12, "maxY": 2.5473684210526315, "series": [{"data": [[1.5490749E12, 2.382608695652173], [1.54907484E12, 2.5473684210526315], [1.54907496E12, 2.3]], "isOverall": false, "label": "Parametros escenario", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907496E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 496.0, "minX": 1.0, "maxY": 2712.0, "series": [{"data": [[8.0, 2712.0], [4.0, 860.0], [2.0, 508.65599999999955], [1.0, 496.0], [9.0, 1682.0], [10.0, 1410.0], [5.0, 1269.5833333333335], [6.0, 2321.0], [3.0, 542.1506849315067], [7.0, 511.0]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}, {"data": [[2.4231884057970996, 563.9072463768116]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 10.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 109.0, "minX": 1.54907484E12, "maxY": 22464.516666666666, "series": [{"data": [[1.5490749E12, 22464.516666666666], [1.54907484E12, 8772.4], [1.54907496E12, 1690.9]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.5490749E12, 1253.4333333333334], [1.54907484E12, 516.0166666666667], [1.54907496E12, 109.0]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907496E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 507.80000000000007, "minX": 1.54907484E12, "maxY": 615.8210526315788, "series": [{"data": [[1.5490749E12, 547.3434782608692], [1.54907484E12, 615.8210526315788], [1.54907496E12, 507.80000000000007]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907496E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 507.75000000000006, "minX": 1.54907484E12, "maxY": 615.5894736842107, "series": [{"data": [[1.5490749E12, 547.1347826086956], [1.54907484E12, 615.5894736842107], [1.54907496E12, 507.75000000000006]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907496E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 362.3999999999999, "minX": 1.54907484E12, "maxY": 473.2947368421051, "series": [{"data": [[1.5490749E12, 394.34347826086963], [1.54907484E12, 473.2947368421051], [1.54907496E12, 362.3999999999999]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907496E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 484.0, "minX": 1.54907484E12, "maxY": 2712.0, "series": [{"data": [[1.5490749E12, 1261.0], [1.54907484E12, 2712.0], [1.54907496E12, 546.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.5490749E12, 485.0], [1.54907484E12, 484.0], [1.54907496E12, 485.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.5490749E12, 541.2], [1.54907484E12, 583.2000000000019], [1.54907496E12, 541.2]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.5490749E12, 2260.339999999998], [1.54907484E12, 2712.0], [1.54907496E12, 2192.9400000000014]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.5490749E12, 1093.4999999999998], [1.54907484E12, 1848.0999999999992], [1.54907496E12, 1070.499999999999]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907496E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 478.0, "minX": 1000.0, "maxY": 509.0, "series": [{"data": [[1000.0, 509.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[1000.0, 478.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1, "maxX": 1000.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.create();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 478.0, "minX": 1000.0, "maxY": 508.0, "series": [{"data": [[1000.0, 508.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[1000.0, 478.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1, "maxX": 1000.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.3, "minX": 1.54907484E12, "maxY": 3.8333333333333335, "series": [{"data": [[1.5490749E12, 3.8333333333333335], [1.54907484E12, 1.6166666666666667], [1.54907496E12, 0.3]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907496E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.016666666666666666, "minX": 1.54907484E12, "maxY": 3.816666666666667, "series": [{"data": [[1.5490749E12, 3.816666666666667], [1.54907484E12, 1.4666666666666666], [1.54907496E12, 0.3333333333333333]], "isOverall": false, "label": "200", "isController": false}, {"data": [[1.5490749E12, 0.016666666666666666], [1.54907484E12, 0.11666666666666667]], "isOverall": false, "label": "429", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907496E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.016666666666666666, "minX": 1.54907484E12, "maxY": 3.816666666666667, "series": [{"data": [[1.5490749E12, 3.816666666666667], [1.54907484E12, 1.4666666666666666], [1.54907496E12, 0.3333333333333333]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated-success", "isController": false}, {"data": [[1.5490749E12, 0.016666666666666666], [1.54907484E12, 0.11666666666666667]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907496E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.016666666666666666, "minX": 1.54907484E12, "maxY": 3.816666666666667, "series": [{"data": [[1.5490749E12, 3.816666666666667], [1.54907484E12, 1.4666666666666666], [1.54907496E12, 0.3333333333333333]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.5490749E12, 0.016666666666666666], [1.54907484E12, 0.11666666666666667]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907496E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "responseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    choiceContainer.find("label").each(function(){
        this.style.color = color;
    });
}

