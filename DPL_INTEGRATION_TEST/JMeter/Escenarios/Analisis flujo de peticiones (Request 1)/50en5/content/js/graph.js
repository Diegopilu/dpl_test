/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 472.0, "minX": 0.0, "maxY": 2727.0, "series": [{"data": [[0.0, 472.0], [0.1, 472.0], [0.2, 472.0], [0.3, 472.0], [0.4, 472.0], [0.5, 472.0], [0.6, 472.0], [0.7, 472.0], [0.8, 472.0], [0.9, 472.0], [1.0, 472.0], [1.1, 472.0], [1.2, 472.0], [1.3, 472.0], [1.4, 472.0], [1.5, 472.0], [1.6, 472.0], [1.7, 472.0], [1.8, 472.0], [1.9, 472.0], [2.0, 473.0], [2.1, 473.0], [2.2, 473.0], [2.3, 473.0], [2.4, 473.0], [2.5, 473.0], [2.6, 473.0], [2.7, 473.0], [2.8, 473.0], [2.9, 473.0], [3.0, 473.0], [3.1, 473.0], [3.2, 473.0], [3.3, 473.0], [3.4, 473.0], [3.5, 473.0], [3.6, 473.0], [3.7, 473.0], [3.8, 473.0], [3.9, 473.0], [4.0, 473.0], [4.1, 473.0], [4.2, 473.0], [4.3, 473.0], [4.4, 473.0], [4.5, 473.0], [4.6, 473.0], [4.7, 473.0], [4.8, 473.0], [4.9, 473.0], [5.0, 473.0], [5.1, 473.0], [5.2, 473.0], [5.3, 473.0], [5.4, 473.0], [5.5, 473.0], [5.6, 473.0], [5.7, 473.0], [5.8, 473.0], [5.9, 473.0], [6.0, 474.0], [6.1, 474.0], [6.2, 474.0], [6.3, 474.0], [6.4, 474.0], [6.5, 474.0], [6.6, 474.0], [6.7, 474.0], [6.8, 474.0], [6.9, 474.0], [7.0, 474.0], [7.1, 474.0], [7.2, 474.0], [7.3, 474.0], [7.4, 474.0], [7.5, 474.0], [7.6, 474.0], [7.7, 474.0], [7.8, 474.0], [7.9, 474.0], [8.0, 479.0], [8.1, 479.0], [8.2, 479.0], [8.3, 479.0], [8.4, 479.0], [8.5, 479.0], [8.6, 479.0], [8.7, 479.0], [8.8, 479.0], [8.9, 479.0], [9.0, 479.0], [9.1, 479.0], [9.2, 479.0], [9.3, 479.0], [9.4, 479.0], [9.5, 479.0], [9.6, 479.0], [9.7, 479.0], [9.8, 479.0], [9.9, 479.0], [10.0, 487.0], [10.1, 487.0], [10.2, 487.0], [10.3, 487.0], [10.4, 487.0], [10.5, 487.0], [10.6, 487.0], [10.7, 487.0], [10.8, 487.0], [10.9, 487.0], [11.0, 487.0], [11.1, 487.0], [11.2, 487.0], [11.3, 487.0], [11.4, 487.0], [11.5, 487.0], [11.6, 487.0], [11.7, 487.0], [11.8, 487.0], [11.9, 487.0], [12.0, 488.0], [12.1, 488.0], [12.2, 488.0], [12.3, 488.0], [12.4, 488.0], [12.5, 488.0], [12.6, 488.0], [12.7, 488.0], [12.8, 488.0], [12.9, 488.0], [13.0, 488.0], [13.1, 488.0], [13.2, 488.0], [13.3, 488.0], [13.4, 488.0], [13.5, 488.0], [13.6, 488.0], [13.7, 488.0], [13.8, 488.0], [13.9, 488.0], [14.0, 490.0], [14.1, 490.0], [14.2, 490.0], [14.3, 490.0], [14.4, 490.0], [14.5, 490.0], [14.6, 490.0], [14.7, 490.0], [14.8, 490.0], [14.9, 490.0], [15.0, 490.0], [15.1, 490.0], [15.2, 490.0], [15.3, 490.0], [15.4, 490.0], [15.5, 490.0], [15.6, 490.0], [15.7, 490.0], [15.8, 490.0], [15.9, 490.0], [16.0, 494.0], [16.1, 494.0], [16.2, 494.0], [16.3, 494.0], [16.4, 494.0], [16.5, 494.0], [16.6, 494.0], [16.7, 494.0], [16.8, 494.0], [16.9, 494.0], [17.0, 494.0], [17.1, 494.0], [17.2, 494.0], [17.3, 494.0], [17.4, 494.0], [17.5, 494.0], [17.6, 494.0], [17.7, 494.0], [17.8, 494.0], [17.9, 494.0], [18.0, 494.0], [18.1, 494.0], [18.2, 494.0], [18.3, 494.0], [18.4, 494.0], [18.5, 494.0], [18.6, 494.0], [18.7, 494.0], [18.8, 494.0], [18.9, 494.0], [19.0, 494.0], [19.1, 494.0], [19.2, 494.0], [19.3, 494.0], [19.4, 494.0], [19.5, 494.0], [19.6, 494.0], [19.7, 494.0], [19.8, 494.0], [19.9, 494.0], [20.0, 495.0], [20.1, 495.0], [20.2, 495.0], [20.3, 495.0], [20.4, 495.0], [20.5, 495.0], [20.6, 495.0], [20.7, 495.0], [20.8, 495.0], [20.9, 495.0], [21.0, 495.0], [21.1, 495.0], [21.2, 495.0], [21.3, 495.0], [21.4, 495.0], [21.5, 495.0], [21.6, 495.0], [21.7, 495.0], [21.8, 495.0], [21.9, 495.0], [22.0, 497.0], [22.1, 497.0], [22.2, 497.0], [22.3, 497.0], [22.4, 497.0], [22.5, 497.0], [22.6, 497.0], [22.7, 497.0], [22.8, 497.0], [22.9, 497.0], [23.0, 497.0], [23.1, 497.0], [23.2, 497.0], [23.3, 497.0], [23.4, 497.0], [23.5, 497.0], [23.6, 497.0], [23.7, 497.0], [23.8, 497.0], [23.9, 497.0], [24.0, 499.0], [24.1, 499.0], [24.2, 499.0], [24.3, 499.0], [24.4, 499.0], [24.5, 499.0], [24.6, 499.0], [24.7, 499.0], [24.8, 499.0], [24.9, 499.0], [25.0, 499.0], [25.1, 499.0], [25.2, 499.0], [25.3, 499.0], [25.4, 499.0], [25.5, 499.0], [25.6, 499.0], [25.7, 499.0], [25.8, 499.0], [25.9, 499.0], [26.0, 504.0], [26.1, 504.0], [26.2, 504.0], [26.3, 504.0], [26.4, 504.0], [26.5, 504.0], [26.6, 504.0], [26.7, 504.0], [26.8, 504.0], [26.9, 504.0], [27.0, 504.0], [27.1, 504.0], [27.2, 504.0], [27.3, 504.0], [27.4, 504.0], [27.5, 504.0], [27.6, 504.0], [27.7, 504.0], [27.8, 504.0], [27.9, 504.0], [28.0, 505.0], [28.1, 505.0], [28.2, 505.0], [28.3, 505.0], [28.4, 505.0], [28.5, 505.0], [28.6, 505.0], [28.7, 505.0], [28.8, 505.0], [28.9, 505.0], [29.0, 505.0], [29.1, 505.0], [29.2, 505.0], [29.3, 505.0], [29.4, 505.0], [29.5, 505.0], [29.6, 505.0], [29.7, 505.0], [29.8, 505.0], [29.9, 505.0], [30.0, 508.0], [30.1, 508.0], [30.2, 508.0], [30.3, 508.0], [30.4, 508.0], [30.5, 508.0], [30.6, 508.0], [30.7, 508.0], [30.8, 508.0], [30.9, 508.0], [31.0, 508.0], [31.1, 508.0], [31.2, 508.0], [31.3, 508.0], [31.4, 508.0], [31.5, 508.0], [31.6, 508.0], [31.7, 508.0], [31.8, 508.0], [31.9, 508.0], [32.0, 510.0], [32.1, 510.0], [32.2, 510.0], [32.3, 510.0], [32.4, 510.0], [32.5, 510.0], [32.6, 510.0], [32.7, 510.0], [32.8, 510.0], [32.9, 510.0], [33.0, 510.0], [33.1, 510.0], [33.2, 510.0], [33.3, 510.0], [33.4, 510.0], [33.5, 510.0], [33.6, 510.0], [33.7, 510.0], [33.8, 510.0], [33.9, 510.0], [34.0, 510.0], [34.1, 510.0], [34.2, 510.0], [34.3, 510.0], [34.4, 510.0], [34.5, 510.0], [34.6, 510.0], [34.7, 510.0], [34.8, 510.0], [34.9, 510.0], [35.0, 510.0], [35.1, 510.0], [35.2, 510.0], [35.3, 510.0], [35.4, 510.0], [35.5, 510.0], [35.6, 510.0], [35.7, 510.0], [35.8, 510.0], [35.9, 510.0], [36.0, 511.0], [36.1, 511.0], [36.2, 511.0], [36.3, 511.0], [36.4, 511.0], [36.5, 511.0], [36.6, 511.0], [36.7, 511.0], [36.8, 511.0], [36.9, 511.0], [37.0, 511.0], [37.1, 511.0], [37.2, 511.0], [37.3, 511.0], [37.4, 511.0], [37.5, 511.0], [37.6, 511.0], [37.7, 511.0], [37.8, 511.0], [37.9, 511.0], [38.0, 513.0], [38.1, 513.0], [38.2, 513.0], [38.3, 513.0], [38.4, 513.0], [38.5, 513.0], [38.6, 513.0], [38.7, 513.0], [38.8, 513.0], [38.9, 513.0], [39.0, 513.0], [39.1, 513.0], [39.2, 513.0], [39.3, 513.0], [39.4, 513.0], [39.5, 513.0], [39.6, 513.0], [39.7, 513.0], [39.8, 513.0], [39.9, 513.0], [40.0, 515.0], [40.1, 515.0], [40.2, 515.0], [40.3, 515.0], [40.4, 515.0], [40.5, 515.0], [40.6, 515.0], [40.7, 515.0], [40.8, 515.0], [40.9, 515.0], [41.0, 515.0], [41.1, 515.0], [41.2, 515.0], [41.3, 515.0], [41.4, 515.0], [41.5, 515.0], [41.6, 515.0], [41.7, 515.0], [41.8, 515.0], [41.9, 515.0], [42.0, 515.0], [42.1, 515.0], [42.2, 515.0], [42.3, 515.0], [42.4, 515.0], [42.5, 515.0], [42.6, 515.0], [42.7, 515.0], [42.8, 515.0], [42.9, 515.0], [43.0, 515.0], [43.1, 515.0], [43.2, 515.0], [43.3, 515.0], [43.4, 515.0], [43.5, 515.0], [43.6, 515.0], [43.7, 515.0], [43.8, 515.0], [43.9, 515.0], [44.0, 517.0], [44.1, 517.0], [44.2, 517.0], [44.3, 517.0], [44.4, 517.0], [44.5, 517.0], [44.6, 517.0], [44.7, 517.0], [44.8, 517.0], [44.9, 517.0], [45.0, 517.0], [45.1, 517.0], [45.2, 517.0], [45.3, 517.0], [45.4, 517.0], [45.5, 517.0], [45.6, 517.0], [45.7, 517.0], [45.8, 517.0], [45.9, 517.0], [46.0, 519.0], [46.1, 519.0], [46.2, 519.0], [46.3, 519.0], [46.4, 519.0], [46.5, 519.0], [46.6, 519.0], [46.7, 519.0], [46.8, 519.0], [46.9, 519.0], [47.0, 519.0], [47.1, 519.0], [47.2, 519.0], [47.3, 519.0], [47.4, 519.0], [47.5, 519.0], [47.6, 519.0], [47.7, 519.0], [47.8, 519.0], [47.9, 519.0], [48.0, 521.0], [48.1, 521.0], [48.2, 521.0], [48.3, 521.0], [48.4, 521.0], [48.5, 521.0], [48.6, 521.0], [48.7, 521.0], [48.8, 521.0], [48.9, 521.0], [49.0, 521.0], [49.1, 521.0], [49.2, 521.0], [49.3, 521.0], [49.4, 521.0], [49.5, 521.0], [49.6, 521.0], [49.7, 521.0], [49.8, 521.0], [49.9, 521.0], [50.0, 529.0], [50.1, 529.0], [50.2, 529.0], [50.3, 529.0], [50.4, 529.0], [50.5, 529.0], [50.6, 529.0], [50.7, 529.0], [50.8, 529.0], [50.9, 529.0], [51.0, 529.0], [51.1, 529.0], [51.2, 529.0], [51.3, 529.0], [51.4, 529.0], [51.5, 529.0], [51.6, 529.0], [51.7, 529.0], [51.8, 529.0], [51.9, 529.0], [52.0, 530.0], [52.1, 530.0], [52.2, 530.0], [52.3, 530.0], [52.4, 530.0], [52.5, 530.0], [52.6, 530.0], [52.7, 530.0], [52.8, 530.0], [52.9, 530.0], [53.0, 530.0], [53.1, 530.0], [53.2, 530.0], [53.3, 530.0], [53.4, 530.0], [53.5, 530.0], [53.6, 530.0], [53.7, 530.0], [53.8, 530.0], [53.9, 530.0], [54.0, 545.0], [54.1, 545.0], [54.2, 545.0], [54.3, 545.0], [54.4, 545.0], [54.5, 545.0], [54.6, 545.0], [54.7, 545.0], [54.8, 545.0], [54.9, 545.0], [55.0, 545.0], [55.1, 545.0], [55.2, 545.0], [55.3, 545.0], [55.4, 545.0], [55.5, 545.0], [55.6, 545.0], [55.7, 545.0], [55.8, 545.0], [55.9, 545.0], [56.0, 555.0], [56.1, 555.0], [56.2, 555.0], [56.3, 555.0], [56.4, 555.0], [56.5, 555.0], [56.6, 555.0], [56.7, 555.0], [56.8, 555.0], [56.9, 555.0], [57.0, 555.0], [57.1, 555.0], [57.2, 555.0], [57.3, 555.0], [57.4, 555.0], [57.5, 555.0], [57.6, 555.0], [57.7, 555.0], [57.8, 555.0], [57.9, 555.0], [58.0, 556.0], [58.1, 556.0], [58.2, 556.0], [58.3, 556.0], [58.4, 556.0], [58.5, 556.0], [58.6, 556.0], [58.7, 556.0], [58.8, 556.0], [58.9, 556.0], [59.0, 556.0], [59.1, 556.0], [59.2, 556.0], [59.3, 556.0], [59.4, 556.0], [59.5, 556.0], [59.6, 556.0], [59.7, 556.0], [59.8, 556.0], [59.9, 556.0], [60.0, 680.0], [60.1, 680.0], [60.2, 680.0], [60.3, 680.0], [60.4, 680.0], [60.5, 680.0], [60.6, 680.0], [60.7, 680.0], [60.8, 680.0], [60.9, 680.0], [61.0, 680.0], [61.1, 680.0], [61.2, 680.0], [61.3, 680.0], [61.4, 680.0], [61.5, 680.0], [61.6, 680.0], [61.7, 680.0], [61.8, 680.0], [61.9, 680.0], [62.0, 755.0], [62.1, 755.0], [62.2, 755.0], [62.3, 755.0], [62.4, 755.0], [62.5, 755.0], [62.6, 755.0], [62.7, 755.0], [62.8, 755.0], [62.9, 755.0], [63.0, 755.0], [63.1, 755.0], [63.2, 755.0], [63.3, 755.0], [63.4, 755.0], [63.5, 755.0], [63.6, 755.0], [63.7, 755.0], [63.8, 755.0], [63.9, 755.0], [64.0, 909.0], [64.1, 909.0], [64.2, 909.0], [64.3, 909.0], [64.4, 909.0], [64.5, 909.0], [64.6, 909.0], [64.7, 909.0], [64.8, 909.0], [64.9, 909.0], [65.0, 909.0], [65.1, 909.0], [65.2, 909.0], [65.3, 909.0], [65.4, 909.0], [65.5, 909.0], [65.6, 909.0], [65.7, 909.0], [65.8, 909.0], [65.9, 909.0], [66.0, 983.0], [66.1, 983.0], [66.2, 983.0], [66.3, 983.0], [66.4, 983.0], [66.5, 983.0], [66.6, 983.0], [66.7, 983.0], [66.8, 983.0], [66.9, 983.0], [67.0, 983.0], [67.1, 983.0], [67.2, 983.0], [67.3, 983.0], [67.4, 983.0], [67.5, 983.0], [67.6, 983.0], [67.7, 983.0], [67.8, 983.0], [67.9, 983.0], [68.0, 1121.0], [68.1, 1121.0], [68.2, 1121.0], [68.3, 1121.0], [68.4, 1121.0], [68.5, 1121.0], [68.6, 1121.0], [68.7, 1121.0], [68.8, 1121.0], [68.9, 1121.0], [69.0, 1121.0], [69.1, 1121.0], [69.2, 1121.0], [69.3, 1121.0], [69.4, 1121.0], [69.5, 1121.0], [69.6, 1121.0], [69.7, 1121.0], [69.8, 1121.0], [69.9, 1121.0], [70.0, 1225.0], [70.1, 1225.0], [70.2, 1225.0], [70.3, 1225.0], [70.4, 1225.0], [70.5, 1225.0], [70.6, 1225.0], [70.7, 1225.0], [70.8, 1225.0], [70.9, 1225.0], [71.0, 1225.0], [71.1, 1225.0], [71.2, 1225.0], [71.3, 1225.0], [71.4, 1225.0], [71.5, 1225.0], [71.6, 1225.0], [71.7, 1225.0], [71.8, 1225.0], [71.9, 1225.0], [72.0, 1375.0], [72.1, 1375.0], [72.2, 1375.0], [72.3, 1375.0], [72.4, 1375.0], [72.5, 1375.0], [72.6, 1375.0], [72.7, 1375.0], [72.8, 1375.0], [72.9, 1375.0], [73.0, 1375.0], [73.1, 1375.0], [73.2, 1375.0], [73.3, 1375.0], [73.4, 1375.0], [73.5, 1375.0], [73.6, 1375.0], [73.7, 1375.0], [73.8, 1375.0], [73.9, 1375.0], [74.0, 1486.0], [74.1, 1486.0], [74.2, 1486.0], [74.3, 1486.0], [74.4, 1486.0], [74.5, 1486.0], [74.6, 1486.0], [74.7, 1486.0], [74.8, 1486.0], [74.9, 1486.0], [75.0, 1486.0], [75.1, 1486.0], [75.2, 1486.0], [75.3, 1486.0], [75.4, 1486.0], [75.5, 1486.0], [75.6, 1486.0], [75.7, 1486.0], [75.8, 1486.0], [75.9, 1486.0], [76.0, 1610.0], [76.1, 1610.0], [76.2, 1610.0], [76.3, 1610.0], [76.4, 1610.0], [76.5, 1610.0], [76.6, 1610.0], [76.7, 1610.0], [76.8, 1610.0], [76.9, 1610.0], [77.0, 1610.0], [77.1, 1610.0], [77.2, 1610.0], [77.3, 1610.0], [77.4, 1610.0], [77.5, 1610.0], [77.6, 1610.0], [77.7, 1610.0], [77.8, 1610.0], [77.9, 1610.0], [78.0, 1657.0], [78.1, 1657.0], [78.2, 1657.0], [78.3, 1657.0], [78.4, 1657.0], [78.5, 1657.0], [78.6, 1657.0], [78.7, 1657.0], [78.8, 1657.0], [78.9, 1657.0], [79.0, 1657.0], [79.1, 1657.0], [79.2, 1657.0], [79.3, 1657.0], [79.4, 1657.0], [79.5, 1657.0], [79.6, 1657.0], [79.7, 1657.0], [79.8, 1657.0], [79.9, 1657.0], [80.0, 1770.0], [80.1, 1770.0], [80.2, 1770.0], [80.3, 1770.0], [80.4, 1770.0], [80.5, 1770.0], [80.6, 1770.0], [80.7, 1770.0], [80.8, 1770.0], [80.9, 1770.0], [81.0, 1770.0], [81.1, 1770.0], [81.2, 1770.0], [81.3, 1770.0], [81.4, 1770.0], [81.5, 1770.0], [81.6, 1770.0], [81.7, 1770.0], [81.8, 1770.0], [81.9, 1770.0], [82.0, 1894.0], [82.1, 1894.0], [82.2, 1894.0], [82.3, 1894.0], [82.4, 1894.0], [82.5, 1894.0], [82.6, 1894.0], [82.7, 1894.0], [82.8, 1894.0], [82.9, 1894.0], [83.0, 1894.0], [83.1, 1894.0], [83.2, 1894.0], [83.3, 1894.0], [83.4, 1894.0], [83.5, 1894.0], [83.6, 1894.0], [83.7, 1894.0], [83.8, 1894.0], [83.9, 1894.0], [84.0, 2052.0], [84.1, 2052.0], [84.2, 2052.0], [84.3, 2052.0], [84.4, 2052.0], [84.5, 2052.0], [84.6, 2052.0], [84.7, 2052.0], [84.8, 2052.0], [84.9, 2052.0], [85.0, 2052.0], [85.1, 2052.0], [85.2, 2052.0], [85.3, 2052.0], [85.4, 2052.0], [85.5, 2052.0], [85.6, 2052.0], [85.7, 2052.0], [85.8, 2052.0], [85.9, 2052.0], [86.0, 2148.0], [86.1, 2148.0], [86.2, 2148.0], [86.3, 2148.0], [86.4, 2148.0], [86.5, 2148.0], [86.6, 2148.0], [86.7, 2148.0], [86.8, 2148.0], [86.9, 2148.0], [87.0, 2148.0], [87.1, 2148.0], [87.2, 2148.0], [87.3, 2148.0], [87.4, 2148.0], [87.5, 2148.0], [87.6, 2148.0], [87.7, 2148.0], [87.8, 2148.0], [87.9, 2148.0], [88.0, 2240.0], [88.1, 2240.0], [88.2, 2240.0], [88.3, 2240.0], [88.4, 2240.0], [88.5, 2240.0], [88.6, 2240.0], [88.7, 2240.0], [88.8, 2240.0], [88.9, 2240.0], [89.0, 2240.0], [89.1, 2240.0], [89.2, 2240.0], [89.3, 2240.0], [89.4, 2240.0], [89.5, 2240.0], [89.6, 2240.0], [89.7, 2240.0], [89.8, 2240.0], [89.9, 2240.0], [90.0, 2374.0], [90.1, 2374.0], [90.2, 2374.0], [90.3, 2374.0], [90.4, 2374.0], [90.5, 2374.0], [90.6, 2374.0], [90.7, 2374.0], [90.8, 2374.0], [90.9, 2374.0], [91.0, 2374.0], [91.1, 2374.0], [91.2, 2374.0], [91.3, 2374.0], [91.4, 2374.0], [91.5, 2374.0], [91.6, 2374.0], [91.7, 2374.0], [91.8, 2374.0], [91.9, 2374.0], [92.0, 2502.0], [92.1, 2502.0], [92.2, 2502.0], [92.3, 2502.0], [92.4, 2502.0], [92.5, 2502.0], [92.6, 2502.0], [92.7, 2502.0], [92.8, 2502.0], [92.9, 2502.0], [93.0, 2502.0], [93.1, 2502.0], [93.2, 2502.0], [93.3, 2502.0], [93.4, 2502.0], [93.5, 2502.0], [93.6, 2502.0], [93.7, 2502.0], [93.8, 2502.0], [93.9, 2502.0], [94.0, 2599.0], [94.1, 2599.0], [94.2, 2599.0], [94.3, 2599.0], [94.4, 2599.0], [94.5, 2599.0], [94.6, 2599.0], [94.7, 2599.0], [94.8, 2599.0], [94.9, 2599.0], [95.0, 2599.0], [95.1, 2599.0], [95.2, 2599.0], [95.3, 2599.0], [95.4, 2599.0], [95.5, 2599.0], [95.6, 2599.0], [95.7, 2599.0], [95.8, 2599.0], [95.9, 2599.0], [96.0, 2657.0], [96.1, 2657.0], [96.2, 2657.0], [96.3, 2657.0], [96.4, 2657.0], [96.5, 2657.0], [96.6, 2657.0], [96.7, 2657.0], [96.8, 2657.0], [96.9, 2657.0], [97.0, 2657.0], [97.1, 2657.0], [97.2, 2657.0], [97.3, 2657.0], [97.4, 2657.0], [97.5, 2657.0], [97.6, 2657.0], [97.7, 2657.0], [97.8, 2657.0], [97.9, 2657.0], [98.0, 2727.0], [98.1, 2727.0], [98.2, 2727.0], [98.3, 2727.0], [98.4, 2727.0], [98.5, 2727.0], [98.6, 2727.0], [98.7, 2727.0], [98.8, 2727.0], [98.9, 2727.0], [99.0, 2727.0], [99.1, 2727.0], [99.2, 2727.0], [99.3, 2727.0], [99.4, 2727.0], [99.5, 2727.0], [99.6, 2727.0], [99.7, 2727.0], [99.8, 2727.0], [99.9, 2727.0]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 400.0, "maxY": 17.0, "series": [{"data": [[2100.0, 1.0], [2300.0, 1.0], [2200.0, 1.0], [600.0, 1.0], [2500.0, 2.0], [2600.0, 1.0], [700.0, 1.0], [2700.0, 1.0], [900.0, 2.0], [1100.0, 1.0], [1200.0, 1.0], [1300.0, 1.0], [1400.0, 1.0], [1600.0, 2.0], [400.0, 13.0], [1700.0, 1.0], [1800.0, 1.0], [500.0, 17.0], [2000.0, 1.0]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 2700.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 4.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 24.0, "series": [{"data": [[0.0, 4.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 24.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 12.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 10.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 11.82, "minX": 1.54907106E12, "maxY": 11.82, "series": [{"data": [[1.54907106E12, 11.82]], "isOverall": false, "label": "Parametros escenario", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907106E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 473.0, "minX": 1.0, "maxY": 2727.0, "series": [{"data": [[2.0, 473.0], [12.0, 909.0], [3.0, 504.0], [13.0, 1375.0], [14.0, 2727.0], [16.0, 1486.0], [4.0, 488.0], [1.0, 490.0], [17.0, 1894.0], [20.0, 1366.0], [5.0, 487.0], [22.0, 1634.5], [6.0, 517.8125], [25.0, 1953.2222222222222], [26.0, 869.0], [7.0, 1610.0]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}, {"data": [[11.82, 998.8399999999999]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 26.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 271.51666666666665, "minX": 1.54907106E12, "maxY": 4073.866666666667, "series": [{"data": [[1.54907106E12, 4073.866666666667]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.54907106E12, 271.51666666666665]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907106E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 998.8399999999999, "minX": 1.54907106E12, "maxY": 998.8399999999999, "series": [{"data": [[1.54907106E12, 998.8399999999999]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907106E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 998.4799999999996, "minX": 1.54907106E12, "maxY": 998.4799999999996, "series": [{"data": [[1.54907106E12, 998.4799999999996]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907106E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 847.7599999999999, "minX": 1.54907106E12, "maxY": 847.7599999999999, "series": [{"data": [[1.54907106E12, 847.7599999999999]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907106E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 494.0, "minX": 1.54907106E12, "maxY": 2727.0, "series": [{"data": [[1.54907106E12, 2727.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.54907106E12, 494.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.54907106E12, 2489.2]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.54907106E12, 2727.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.54907106E12, 2654.1]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907106E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 483.0, "minX": 1000.0, "maxY": 1959.0, "series": [{"data": [[1000.0, 537.5], [2000.0, 1959.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[1000.0, 483.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1, "maxX": 2000.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.create();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 483.0, "minX": 1000.0, "maxY": 1959.0, "series": [{"data": [[1000.0, 537.0], [2000.0, 1959.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[1000.0, 483.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1, "maxX": 2000.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.8333333333333334, "minX": 1.54907106E12, "maxY": 0.8333333333333334, "series": [{"data": [[1.54907106E12, 0.8333333333333334]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907106E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.16666666666666666, "minX": 1.54907106E12, "maxY": 0.6666666666666666, "series": [{"data": [[1.54907106E12, 0.6666666666666666]], "isOverall": false, "label": "200", "isController": false}, {"data": [[1.54907106E12, 0.16666666666666666]], "isOverall": false, "label": "429", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.54907106E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.16666666666666666, "minX": 1.54907106E12, "maxY": 0.6666666666666666, "series": [{"data": [[1.54907106E12, 0.6666666666666666]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated-success", "isController": false}, {"data": [[1.54907106E12, 0.16666666666666666]], "isOverall": false, "label": "TopRated9 /3/movie/top_rated-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907106E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.16666666666666666, "minX": 1.54907106E12, "maxY": 0.6666666666666666, "series": [{"data": [[1.54907106E12, 0.6666666666666666]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.54907106E12, 0.16666666666666666]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.54907106E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "responseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    choiceContainer.find("label").each(function(){
        this.style.color = color;
    });
}

