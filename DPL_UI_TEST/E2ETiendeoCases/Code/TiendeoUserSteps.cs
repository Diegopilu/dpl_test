﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;
using NUnit.Framework;
using TechTalk.SpecRun;
using System.Collections.Generic;
using System.Threading;

namespace E2ETiendeoCases.Code
{
    [Binding]
    public class TiendeoUserSteps
    {
        IWebDriver driver;

        [Given(@"That I am on the webside")]
        public void GivenThatIAmOnTheWebside()
        {
            driver = new FirefoxDriver();
            driver.Url = "https://www.tiendeo.com/";
        }
        
        [When(@"I click on the sports section")]
        public void WhenIClickOnTheSportsSection()
        {
            driver.FindElement(By.XPath("/html/body/header/nav/div/div[1]/ul[3]/li/ul/li[1]/ul/li[8]/a/span[2]")).Click();
        }
        
        [Then(@"I see the top sports catalog")]
        public void ThenISeeTheTopSportsCatalog()
        {
            Assert.That(driver.FindElement(By.XPath("/html/body/div[1]/div[1]/div[5]/div/div[1]/div[1]/article[3]/div")).Displayed);

            driver.Quit();
        }

        [When(@"I click on the restaurant section")]
        public void WhenIClickOnTheRestaurantSection()
        {
           driver.FindElement(By.XPath("/html/body/header/nav/div/div[1]/ul[3]/li/ul/li[1]/ul/li[11]/a/span[2]")).Click();            
        }

        [Then(@"I view BurguerKing catalog")]
        public void ThenIviewBurguerKingcatalog()
        {
           Assert.That(driver.FindElement(By.XPath("//a[@class='list-group-item retailerLi'][contains(.,'Burger King')]")).Displayed);
           driver.Quit();
        }

        [Given(@"I click in my Tiendeo menu")]
        public void GivenIclickinmyTiendeomenu()
        {
            driver.FindElement(By.XPath("//span[@class='dropdown-toggle yamm-dropdown-link'][contains(.,'Mi Tiendeo')]")).Click();
        }

        [When(@"I click in favorite link")]
        public void WhenIclickinfavoritelink()
        {
            driver.FindElement(By.XPath("//a[contains(.,'Favoritos')]")).Click();
        }

        [Then(@"I can view my favorites shop")]
        public void ThenIcanviewmyfavoritesshop()
        {
            Assert.That(driver.FindElement(By.XPath("//img[contains(@src,'https://static0.tiendeo.com/upload_negocio/negocio_43/logo2.png')]")).Displayed);
            Thread.Sleep(4000);
            driver.Quit();
        }


        [Given(@"I scroll down to the bottom of the page")]
        public void GivenIscrolldowntothebottomofthepage()
        {
            
            driver.FindElement(By.XPath("/html/body/div[11]/section/div/div[1]/ul/li[5]/a")).SendKeys(Keys.PageDown);
            Thread.Sleep(2000);

        }

        [When(@"I go to contact link")]
        public void WhenIgotocontactlink()
        {
            driver.Url = driver.FindElement(By.XPath("//html/body/div[11]/section/div/div[1]/ul/li[5]/a")).GetAttribute("href");
        }

        [Then(@"I find the telephone number of Tiendeo")]
        public void ThenIfindthetelephonenumberofTiendeo()
        {

            Thread.Sleep(6000);
            //driver.FindElement(By.XPath("/html/body/div[1]/div/div/div/main/article/div[2]/div/div[2]/div[1]/div/p[2]/span")).GetAttribute("span");
            Assert.That(driver.FindElement(By.XPath("//span[contains(.,'Tel. 93 178 0715')]")).Displayed);


            driver.Quit();
        }
    }
}
